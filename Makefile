# Executables
latexmk = latexmk

# Main file name
MAIN = IR
MASTER = main
WORKING_DIR = src/

PRESENTATION = handout beamer

all: $(PRESENTATION) article

$(PRESENTATION):
	cd $(WORKING_DIR) && \
	mkdir -vp ../out && \
	rm -vf $(MASTER).presentation.pdf && \
	$(latexmk) -interaction=nonstopmode -shell-escape -synctex=1 -lualatex -usepretex='\providecommand{\outputformat}{$@}' $(MASTER).presentation.tex && \
	cp $(MASTER).presentation.pdf ../out/$(MAIN)-$@.pdf

article: beamer
	cd $(WORKING_DIR) && \
	mkdir -vp ../out && \
	$(latexmk) -interaction=nonstopmode -shell-escape -synctex=1 -lualatex $(MASTER).$@.tex && \
	cp $(MASTER).$@.pdf ../out/$(MAIN)-$@.pdf

check: all
	# At the moment beamer doesn't support PDF/A-3b because hyperref
	# TODO:
	#  - check if all figures have fonts embedded
	#  - check if out/*-{presentation,handout}.pdf are a PDF/A-3b
	#  - check if out/*-{article,handout}.pdf is a PDF/X-410

.PHONY: check
