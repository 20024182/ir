<div align="center">
<div>
<a href="https://gitlab.di.unipmn.it/20024182/ir/-/jobs/artifacts/main/raw/out/IR-beamer.pdf?job=compile"><img src="https://img.shields.io/badge/download-presentation-green?style=for-the-badge" alt="presentation"></a>
<a href="https://gitlab.di.unipmn.it/20024182/ir/-/jobs/artifacts/main/raw/out/IR-article.pdf?job=compile"><img src="https://img.shields.io/badge/download-article-green?style=for-the-badge" alt="article"></a>
<a href="https://gitlab.di.unipmn.it/20024182/ir/-/jobs/artifacts/main/raw/out/IR-handout.pdf?job=compile"><img src="https://img.shields.io/badge/download-handout-green?style=for-the-badge" alt="handout"></a>
</div>
</div>

# Information Retrieval

Presentazione sul word2vec per l'esame di Information Retrieval (IR) del prof. Ruffo (A.A.2021/22).

Nonché ottimo esempio, per studenti e insegnanti, di uso della classe LaTeX [`beamer`](https://ctan.org/pkg/beamer).
Qualora si volesse preparare le slide per le lezioni di un intero corso è necessario dividerle in `\part` e `\lecture`.

Si precisa che _non è stata curata_ la "versione articolo", perché non richiesta per l'esame.

## Jupyter Notebook

## Creation of virtual environment

Unfortunately, to `whatlies` requires `gensim~=3.8.3` so it is necessary to install a Python version &le; 3.9.x.
It can be easily achieved with using [`pyenv`](https://github.com/pyenv/pyenv).

```shell
$ pyenv install -v 3.9.x
$ PYENV_VERSION=3.9.x pyenv exec python -m venv --upgrade-deps '.venv'
$ source .venv/bin/activate
$ pip -vvv install --upgrade wheel
```

### Download model

I advise to you to use [`aria2c`](https://aria2.github.io/) to download the model.

```shell
$ aria2c -x16 'https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.en.300.bin.gz'
$ gzip -vd file.gz
```

### Python deps
```shell
$ sudo pacman -S gcc12 # for Arch based OSs
$ CC=gcc-12 pip -vvv install --upgrade -r ./requirements.txt
```

### Save PDFs

At the moment of writing `altair_saver` requires a `npm` version that supports `npm bin`, the last one is 8.19.4.

```shell
$ npm -ddd install -g npm@8.19.4
$ npm --version # optional
$ npm -ddd install
$ npm -ddd update
$ npm -ddd audit fix
```

### Run notebook
```shell
$ python -m notebook --no-browser
```
